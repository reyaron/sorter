import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class FileSorter{

    private static final String FILE_NAME = "numbers.txt";
    private static final int PARALLELISM = 10;
    private static final int BULK_SIZE = 10;

    public static void main(String[] args) throws IOException {
        Integer[] fileNumbers = getFileNumbers();
        sortFileBulks(fileNumbers, PARALLELISM, BULK_SIZE).block();
        int numOfBulksToMerge = fileNumbers.length / BULK_SIZE;
        for (int i = 1; i < numOfBulksToMerge + 1; i++) {
            mergeSubArrays(fileNumbers, 0, (i * BULK_SIZE)-1 , ((i+1) * BULK_SIZE)-1);
        }
        Arrays.stream(fileNumbers).forEach(a -> {
            System.out.println(a);
        });
    }

    private static Integer[] getFileNumbers() throws IOException {
        Integer[] numbers = (Integer[]) Array.newInstance(Integer.class, 1000);
        File f = new File(FILE_NAME);
        AtomicInteger index = new AtomicInteger(0);
        Files.lines(Paths.get(f.getAbsolutePath())).forEach(s -> {
            numbers[index.getAndIncrement()] = Integer.valueOf(s);
        });
        return numbers;
    }

    private static Mono<Void> sortFileBulks(Integer[] fileLines, int parallelism, int bulkSize) {
        AtomicInteger bulkIndex = new AtomicInteger(0);
        Mono<Void> sortedBulks = Flux.fromArray(fileLines)
                .window(bulkSize)
                .concatMap(Flux::collectList)
                .flatMap(b -> Flux.just(sort(b, fileLines, bulkSize, bulkIndex.getAndIncrement())))
                .then()
                .subscribeOn(Schedulers.fromExecutor(Executors.newFixedThreadPool(parallelism)));
        return sortedBulks;
    }

    private static Mono<Void> sort(List<Integer> b, Integer[] fileLines, int bulkSize, int bulkIndex) {
        Collections.sort(b);
        int start = bulkSize * bulkIndex;
        for (int i = 0; i < bulkSize && (i+start)< fileLines.length; i++) {
            fileLines[i + start] = b.get(i);
        }
        return Mono.empty();
    }

    static void mergeSubArrays(Integer fileLines[], int startFirst, int startSecond, int endSecond)
    {
        int start2 = startSecond + 1;
        if (start2 >= fileLines.length || fileLines[startSecond] <= fileLines[start2]) {
            return;
        }
        while (startFirst <= startSecond && start2 <= endSecond) {
            if (fileLines[startFirst] <= fileLines[start2]) {
                startFirst++;
            }
            else {
                int value = fileLines[start2];
                int index = start2;
                while (index != startFirst) {
                    fileLines[index] = fileLines[index - 1];
                    index--;
                }
                fileLines[startFirst] = value;
                startFirst++;
                startSecond++;
                start2++;
            }
        }
    }


}
